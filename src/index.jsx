import { render } from 'preact';
import preactLogo from './assets/preact.svg';
import './style.css';
import { useDropzone } from 'react-dropzone';
import { useCallback } from 'preact/compat';

export function App() {
    const onDrop = useCallback(acceptedFiles => {
        console.log(acceptedFiles);
        // Do something with the files
    }, []);
    const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

    return (
        <div>
            <a href="https://preactjs.com" target="_blank">
                <img src={preactLogo} alt="Preact logo" height="160" width="160" />
            </a>
            <h1>Hello world from preact</h1>
            <div {...getRootProps()}>
                <input {...getInputProps()} />
                {isDragActive ? (
                    <p>Drop the files here ...</p>
                ) : (
                    <p>Drag 'n' drop some files here, or click to select files</p>
                )}
            </div>
        </div>
    );
}

function Resource(props) {
    return (
        <a href={props.href} target="_blank" class="resource">
            <h2>{props.title}</h2>
            <p>{props.description}</p>
        </a>
    );
}

render(<App />, document.getElementById('app'));
